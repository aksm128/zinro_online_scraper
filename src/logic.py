# 本当はtyping使った方がいいけどサーバーがpython3.7なので消している

class Game:
	id: int
	name: str
	numbering: int


class Player:
	room_id: int
	name: str
	trip: str
	job: str
	status: str
	won: bool


class Message:
	room_id: int
	from_user: str
	to_user: str
	message: str
	created: str


job_team_table = {
	"村人": ["村人", "占い師", "霊能者", "狩人", "猫又", "共有者", "狼憑き", "怪盗", "ものまね", "役人"],
	"人狼": ["人狼", "狂人", "狂信者"],
	"妖狐": ["妖狐", "背徳者"],
	"てるてる": ["てるてる"]
}


def get_won_jobs(won_team):
	return job_team_table[won_team]
