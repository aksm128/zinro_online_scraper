def messages_to_csv(messages):
	csv = ""
	for message in messages:
		csv += "{},{},{},{}\n".format(message["from_user"], message["to_user"], message["message"], message["created"])
	return csv
