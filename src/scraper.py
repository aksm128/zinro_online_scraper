from zinro_online_tools.log import fetch_log
import requests
from time import sleep


class Scraper:
	def __init__(self, interval=2):
		self.session = requests.session()
		self.interval = interval

	def scrape(self, room_id):
		sleep(self.interval)
		return fetch_log(room_id, session=self.session)
