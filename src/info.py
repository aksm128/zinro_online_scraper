import json
import os


class InformationJson:
	def __init__(self, path):
		self.path = path

	def exists(self):
		return os.path.exists(self.path)

	def save(self, content):
		with open(self.path, "w") as f:
			json.dump(content, f)

	def load(self):
		with open(self.path, "r") as f:
			return json.load(f)
