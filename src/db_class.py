from abc import ABCMeta, abstractmethod
from logic import Game, Player


class LogDB(metaclass=ABCMeta):
	@abstractmethod
	def __init__(self):
		pass

	@abstractmethod
	def commit(self):
		pass

	@abstractmethod
	def delete(self):
		pass

	@abstractmethod
	def init_without_delete(self):
		pass

	@abstractmethod
	def insert_game(self, game: Game):
		pass

	@abstractmethod
	def insert_player(self, player: Player):
		pass
