import datetime
import re


class BrokenLogError(Exception):
	pass


def sort_messages(messages):
	start_time = datetime.datetime.strptime(messages[0]["created"], '%Y-%m-%d %H:%M:%S')
	end_time = datetime.datetime.strptime(messages[-1]["created"], '%Y-%m-%d %H:%M:%S')
	if start_time > end_time:
		messages = messages[::-1]
	return messages


def print_messages(messages):
	for message in messages:
		print("{}: {}".format(message["from_user"], message["message"]))


# 途中からゲームを始めた場合RM取得が不可能 再帰的に調べればできるけど面倒くさい
# def find_rm_name(messages):
# 	first_message = messages[0]
# 	if first_message["from_user"] != "鯖":
# 		raise BrokenLogError()
# 	name_match = re.match("(.*?)さんが村を作成しました。", first_message["message"])
# 	if not name_match:
# 		raise BrokenLogError()
# 	return name_match.groups()[0]


def find_last_game_prorogue_index(messages):
	last_game_prorogue = 0
	for i, message in enumerate(messages):
		if message["from_user"] == "鯖" and message["message"] == "ゲームを開始します":
			last_game_prorogue = i
	return last_game_prorogue


def parse_jobset(jobset_message):
	jobset_match = re.match("役職設定【(.*?),役欠け:(.*?)】", jobset_message["message"])
	yakukake = jobset_match.groups()[1] == "あり"
	jobset = {
		job_pair[0]: job_pair[1] for job_pair in [
			job_info.split("-") for job_info in jobset_match.groups()[0].split(",")
		]
	}
	return {
		"yakukake": yakukake,
		"jobs": jobset
	}


def find_won_team(messages):
	for message in messages:
		if message["from_user"] == "鯖" and "の勝利です" in message["message"]:
			return re.search("【(.*?)(チーム)?】の勝利です", message["message"]).groups()[0]


def analyze_messages(messages):
	messages = sort_messages(messages)
	# print_messages(messages)
	last_game_prorogue_index = find_last_game_prorogue_index(messages)
	jobs = parse_jobset(messages[last_game_prorogue_index - 1])
	won_team = find_won_team(messages[last_game_prorogue_index:])
	return {
		"jobset": jobs,
		"won_team": won_team,
	}
