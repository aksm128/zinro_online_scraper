import sqlite3
from db_class import LogDB
from logic import Game, Player, Message


class SQLiteDB(LogDB):
	def __init__(self, path):
		super(SQLiteDB, self).__init__()
		self.connection = sqlite3.connect(path)
		self.db = self.connection.cursor()

	def __del__(self):
		self.connection.close()

	def commit(self):
		self.connection.commit()

	def delete(self):
		self.db.execute("""
		DROP TABLE IF EXISTS game;
		""")
		self.db.execute("""
		DROP TABLE IF EXISTS player;
		""")

	def init_without_delete(self):
		self.db.execute("""
		CREATE TABLE IF NOT EXISTS game (
			id INTEGER PRIMARY KEY NOT NULL, 
			name TEXT NOT NULL, 
			numbering INTEGER
		);
		""")

		self.db.execute("""
		CREATE INDEX IF NOT EXISTS game_name_index on game(name);
		""")

		self.db.execute("""
		CREATE TABLE IF NOT EXISTS player (
			id INTEGER NOT NULL, 
			name TEXT NOT NULL,
			trip TEXT,
			job TEXT,
			status TEXT,
			won INT
		);
		""")

		self.db.execute("""
		CREATE INDEX IF NOT EXISTS player_name_index on player(name);
		""")

	def insert_game(self, game: Game):
		self.db.execute("""
		INSERT INTO game VALUES (?, ?, ?);
		""", (game["id"], game["name"], game["numbering"]))

	def insert_player(self, player: Player):
		self.db.execute("""
		INSERT INTO player VALUES (?, ?, ?, ?, ?, ?);
		""", (player["room_id"], player["name"], player["trip"], player["job"], player["status"], player["won"]))
