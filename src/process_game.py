from analyze_messages import analyze_messages
from logic import get_won_jobs


def process_game(game):
	result = analyze_messages(game["messages"])
	won_job = get_won_jobs(result["won_team"])
	for i, player in enumerate(game["players"]):
		game["players"][i]["won"] = player["job"] in won_job
		game["players"][i]["room_id"] = game["id"]
	for i, message in enumerate(game["messages"]):
		game["messages"][i]["room_id"] = game["id"]
	return game


if __name__ == "__main__":
	from zinro_online_tools.log import fetch_log
	print(process_game(fetch_log(600000)))
