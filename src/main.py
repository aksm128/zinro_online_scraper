from sqlite_db import SQLiteDB
from scraper import Scraper
from process_game import process_game
from info import InformationJson
from zinro_online_tools.log import LogStatus
from zinro_online_tools.room_list import fetch_room_list
from messages_to_csv import messages_to_csv
import gzip
import traceback


def main():
	scraper = Scraper()
	db = SQLiteDB("./resources/scrape_data.sqlite3")
	db.init_without_delete()

	info_file = InformationJson("./resources/scrape_info.json")
	if not info_file.exists():
		info_file.save({
			"last_scrape_room_id": -1
		})

	error_file = InformationJson("./resources/scrape_error.json")
	if not error_file.exists():
		error_file.save([])

	last_scrape_room_id = info_file.load()["last_scrape_room_id"]

	latest_room_id = int(fetch_room_list("終了")[0]["id"])

	for room_id in range(last_scrape_room_id + 1, latest_room_id + 1):
		print(room_id)
		try:
			data = scraper.scrape(room_id)
			if data["error"] != LogStatus.public:
				info_file.save({
					"last_scrape_room_id": room_id
				})
				continue
			result = process_game(data)
			db.insert_game({
				"id": result["id"],
				"name": result["room_name"],
				"numbering": result["numbering"]
			})
			for player in result["players"]:
				db.insert_player(player)
			csv = messages_to_csv(result["messages"])
			with gzip.open("./resources/messages/game_{}.csv.gz".format(room_id), "wb") as f:
				f.write(csv.encode("utf-8"))
			db.commit()
			info_file.save({
				"last_scrape_room_id": room_id
			})
		except Exception:
			print(traceback.format_exc())
			error_log = error_file.load()
			error_log.append({"id": room_id, "error": traceback.format_exc()})
			error_file.save(error_log)
			info_file.save({
				"last_scrape_room_id": room_id
			})
		except KeyboardInterrupt:
			break


if __name__ == "__main__":
	main()
